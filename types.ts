export type Channel = {
    channelID: string;
    channelName: string;
    description: string;
    subscriberCount: number;
    viewCount: number;
    videoCount: number;
    thumbnail: string;
  };
  
export  type Video = {
    videoID: string;
    title: string;
    description: string;
    viewCount: number;
    likeCount: number;
    commentCount: number;
    thumbnail: string;
    channelID: string;
    channelName: string;
    channelThumbnail: string;
  };
  
export  type Playlist = {
    playlistID: string;
    channelID: string;
    title: string;
    publishedAt: string;
    description: string;
    videoCount: number;
    thumbnail: string;
    totalComments: number;
    totalLikes: number;
    totalViews: number;
    channelName: string;
    channelThumbnail: string;
  };


export type SearchParams = {
  searchValue: string;
  filterValue: string;
  minValue: string;
  maxValue: string;
  sortValue: string;
};

export type AboutStats = {
  adrianCommits: number;
  adrianIssues: number;
  alecCommits: number;
  alecIssues: number;
  junyuCommits: number;
  junyuIssues: number;
  nirmalCommits: number;
  nirmalIssues: number;
  totalCommits: number;
  totalIssues: number;
};

export type TeamMember = {
  name: string;
  imageSrc: string;
  role: string;
  description?: string;
  commitKey: keyof AboutStats;
  issueKey: keyof AboutStats;
  testKey: string;
};
