// pages/index.tsx
import React, { useEffect, useState } from 'react';
import Layout from '../components/Layout';
import Carousel from '../components/Carousel';
import axios from 'axios';
import { Channel, Video, Playlist } from '../types';
import Link from 'next/link';


const IndexPage: React.FC = () => {
  const [channels, setChannels] = useState<Channel[]>([]);
  const [videos, setVideos] = useState<Video[]>([]);
  const [playlists, setPlaylists] = useState<Playlist[]>([]);

  useEffect(() => {
    const fetchChannels = async () => {
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/channels/1`);
        setChannels(response.data.channels);
      } catch (error) {
        console.error('Error fetching channels:', error);
      }
    };

    const fetchVideos = async () => {
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/videos/1`);
        setVideos(response.data.videos);
      } catch (error) {
        console.error('Error fetching videos:', error);
      }
    };

    const fetchPlaylists = async () => {
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/playlists/1`);
        setPlaylists(response.data.playlists);
      } catch (error) {
        console.error('Error fetching playlists:', error);
      }
    };

    fetchChannels();
    fetchVideos();
    fetchPlaylists();
  }, []);

  return (
    <Layout>
      <div className="container mt-4" style={{ backgroundColor: 'black', borderRadius: '10px' }}>
        <h1 style={{color:'white',justifyContent:'center'}}> Welcome to YtubeDB</h1>
        {/* Channels Carousel */}
        <section id="channels" className="mt-4">
          <Link href="/channels" className="titleHover" style={{ color: 'white', textDecoration: 'none' }}>
            <h1 >Channels</h1>
          </Link>
          <Carousel items={channels.map(channel => ({
            id: channel.channelID,
            snippet: {
              title: channel.channelName,
              thumbnails: { high: { url: channel.thumbnail }},
              description: channel.description,
              itemType: 'channel',
            },
          }))} carouselId="channelsCarousel" />
        </section>

        {/* Videos Carousel */}
        <section id="videos" className="mt-4">
          <Link href="/videos" className="titleHover" style={{ color: 'white', textDecoration: 'none' }}>
            <h1>Videos</h1>
          </Link>
          <Carousel items={videos.map(video => ({
            id: video.videoID,
            snippet: {
              title: video.title,
              thumbnails: { high: { url: video.thumbnail }},
              description: video.description,
              itemType: 'video',
            },
          }))} carouselId="videosCarousel" />
        </section>

        {/* Playlists Carousel */}
        <section id="playlists" className="mt-4">
          <Link href="playlists/" className="titleHover" style={{ color: 'white', textDecoration: 'none' }}>
            <h1>Playlists</h1>
          </Link>
          <Carousel items={playlists.map(playlist => ({
            id: playlist.playlistID,
            snippet: {
              title: playlist.title,
              thumbnails: { high: { url: playlist.thumbnail }},
              description: playlist.description,
              itemType: 'playlist',
            },
          }))} carouselId="playlistsCarousel" />
        </section>

        {/* About Section */}
        <section id="about" className="mt-4">
          <Link href="/about" className="titleHover" style={{ color: 'white', textDecoration: 'none' }}>
              <h2>About</h2>
              <p>Learn more about YtubeDB and how it works.</p>
          </Link>
        </section>
      </div>
    </Layout>
  );
};

export default IndexPage;
