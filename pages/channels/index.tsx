import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Layout from '../../components/Layout';
import axios from 'axios';
import SearchFilterSort from '../../components/SearchFilterSort';
import Pagination from '@/components/Pagination';
import { Channel } from '../../types';


const ChannelsPage: React.FC = () => {
  const [channels, setChannels] = useState<Channel[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [totalPages, setTotalPages] = useState(0);
  const [searchTerm, setSearchTerm] = useState('');
  const [filter, setFilter] = useState('');
  const [minArg, setMinArg] = useState('');
  const [maxArg, setMaxArg] = useState('');
  const [sort, setSort] = useState('');
  const [sortOrder, setSortOrder] = useState('asc');


  useEffect(() => {
    const fetchChannels = async () => {
      setLoading(true);
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/channels/${currentPage}`, {
          params: {
            search_arg: searchTerm,
            filter_arg: filter,
            filter_min_arg: minArg,
            filter_max_arg: maxArg,
            sort_arg: sort,
            sort_ord: sortOrder
          }
        });
        setChannels(response.data.channels); 
        setTotalPages(response.data.total_pages);
      } catch (error) {
        console.error('Error fetching channels:', error);
        setChannels([]);
      } finally {
        setLoading(false);
      }
    };

    fetchChannels();
  }, [currentPage, searchTerm, filter, minArg, maxArg, sort, sortOrder]);

  const handleSearchChange = (searchValue:string) => {
    setSearchTerm(searchValue);
  };

  const handleFilterChange = (filterValue:string, minValue:string='', maxValue:string='') => {
    setFilter(filterValue);
    setMinArg(minValue);
    setMaxArg(maxValue);
  };

  const handleSortChange = (sortValue:string) => {
    setSort(sortValue);
  };

  const handleSortOrderChange = (orderValue: 'asc' | 'desc') => {
    setSortOrder(orderValue);
  };

  const maxPageNumbersToShow = 1;
  let startPage = Math.max(currentPage - Math.floor(maxPageNumbersToShow / 2), 1);
  let endPage = startPage + maxPageNumbersToShow - 1;

  // Adjust if endPage exceeds total pages
  if (endPage > totalPages) {
    endPage = totalPages;
    // Ensure that we always display maxPageNumbersToShow (if possible)
    startPage = Math.max(1, endPage - maxPageNumbersToShow + 1);
  }

  const pageNumbers = [];
  for (let number = startPage; number <= endPage; number++) {
    pageNumbers.push(number);
  }
  return (
    <>
      <Head>
        <title>Channel List</title>
      </Head>
      <Layout>
        <main className="py-5 mt-5">
          <div className="container">
            <h1 className="fw-light text-center">YouTube Channels</h1>
            <SearchFilterSort
              onSearchChange={handleSearchChange}
              onFilterChange={handleFilterChange}
              onSortChange={handleSortChange}
              onSortOrderChange={handleSortOrderChange}
              filterOptions={[{value: 'subscribers', label: 'subscribers'}, {value: 'video count', label: 'video count'}, {value: 'views', label: 'views'}]}
              sortOptions={[{value: 'title', label: 'title'}, {value: 'subscribers', label: 'subscribers'}, {value: 'video count', label: 'video count'}, {value: 'views', label: 'views'}, {value: 'creation date', label: 'creation date'}]}
            />
            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              setCurrentPage={setCurrentPage}
            />
            {loading ? (
              <div className="text-center">
                <span>Loading...</span>
              </div>
            ) : (
              <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 mt-3">
                {channels.map((channel) => (
                  <div className="col" key={channel.channelID}>
                    <div className="card shadow-sm">
                      <Link href={`/channels/${channel.channelID}`}>
                          <img className="bd-placeholder-img card-img-top" src={channel.thumbnail} alt="Channel Thumbnail" />
                      </Link>
                      <div className="card-body">
                        <h5 className="card-title">{channel.channelName}</h5>
                        <br />
                        <div className="d-flex justify-content-between align-items-center">
                          <p><i className="fas fa-video"></i> {channel.videoCount} </p>
                          <p><i className="fas fa-user-friends"></i> {channel.viewCount} </p>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            )}
            <div className="d-flex justify-content-center mt-4">
              <Pagination
                currentPage={currentPage}
                totalPages={totalPages}
                setCurrentPage={setCurrentPage}
              />
            </div>
          </div>
        </main>
      </Layout>
    </>
  );
};

export default ChannelsPage;
