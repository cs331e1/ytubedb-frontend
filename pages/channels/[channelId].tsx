import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import axios from 'axios';
import Layout from '../../components/Layout';
import { useRouter } from 'next/router';
import { Channel } from '../../types';


const ChannelPage = () => {
  const [channel, setChannel] = useState<Channel | null>(null);
  const [error, setError] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const router = useRouter();
  const { channelId } = router.query;
  const [videos, setVideos] = useState<any[]>([]);
  const [playlists, setPlaylists] = useState<any[]>([]); 
  const [showDescription, setShowDescription] = useState(false);
  const [showVideos, setShowVideos] = useState(false);
  const [showPlaylists, setShowPlaylists] = useState(false);

  useEffect(() => {
    const fetchChannel = async () => {
      if (!channelId) {
        setIsLoading(false);
        return; // Exit early if channelId is not available
      }
      setIsLoading(true); // Start loading
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/channel/${channelId}`);
        if (!response.data.channel) {
          setError('Channel data is not in expected format or not found.');
        } else {
          setChannel(response.data.channel);
          setVideos(response.data.videos);
          setPlaylists(response.data.playlists);
        }
      } catch (error) {
        console.error(error);
        setError('Failed to fetch channel data.');
      } finally {
        setIsLoading(false); // Stop loading regardless of the outcome
      }
    };

    fetchChannel();
  }, [channelId]); // Depend on channelId

  return (
    <>
      <Head>
        <title>{channel ? channel.channelName : 'Channel Not Found'}</title>
      </Head>
      <Layout>
      <main className="py-5 mt-5">
        <div className="container">
          {isLoading ? (
            <h1 className="fw-light text-center">Loading...</h1>
          ) : error ? (
            <h1 className="fw-light text-center">Error: {error}</h1>
          ) : channel ? (
            <>
              <h1 className="fw-light text-center">{channel.channelName}</h1>
              <div className="card mb-3">
                <div className="d-flex justify-content-center">
                  <img src={channel.thumbnail} alt={channel.channelName} className="img-fluid" style={{ maxWidth: '100%', height: 'auto', marginBottom: '20px', borderRadius: '8px', boxShadow: '0 4px 6px rgba(0,0,0,0.1)' }} />
                </div>
                <div className="card-body">
                  <div className="d-flex justify-content-between align-items-center">
                    <p className="card-title">
                      <i className="far fa-eye"></i> {channel.viewCount.toLocaleString()}
                    </p>
                    <p className="card-title">
                      <i className="fas fa-user-friends"></i> {channel.subscriberCount.toLocaleString()}
                    </p>
                    <p className="card-title">
                      <i className="fas fa-video"></i> {channel.videoCount}
                    </p>
                  </div>
                  <div className="mt-3"><i className="fas fa-list"></i>  {channel.description}</div>
                  <button className="btn btn-outline-secondary w-100 mt-3" type="button" onClick={() => setShowVideos(!showVideos)}>
                    View Videos
                  </button>
                  {showVideos && (
                    <div className="accordion-body">
                      {videos.length > 0 ? (
                        <ul className="list-unstyled">
                          {videos.map((video) => (
                            <li key={video.videoID}>
                              <Link href={`/videos/${video.videoID}`}>
                                <div className="ml-2 w-100 mt-3">{video.title}</div>
                              </Link>
                            </li>
                          ))}
                        </ul>
                      ) : (
                        <p>No videos found for this channel.</p>
                      )}
                    </div>
                  )}
                  <button className="btn btn-outline-secondary w-100 mt-3" type="button" onClick={() => setShowPlaylists(!showPlaylists)}>
                    View Playlists
                  </button>
                  {showPlaylists && (
                    <div className="accordion-body">
                      {playlists.length > 0 ? (
                        <ul className="list-unstyled">
                          {playlists.map((playlist) => (
                            <li key={playlist.playlistID}>
                              <Link href={`/playlists/${playlist.playlistID}`}>
                                <div className='ml-2 w-100 mt-3'>{playlist.title}</div>
                              </Link>
                            </li>
                          ))}
                        </ul>
                      ) : (
                        <p>This channel does not have any playlists.</p>
                      )}
                    </div>
                  )}
                </div>
              </div>
            </>
          ) : (
            <h1 className="fw-light text-center">Sorry, this channel is not in our database.</h1>
          )}
        </div>
      </main>
      </Layout>
    </>
  );
};

export default ChannelPage;

