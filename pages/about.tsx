import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import axios from 'axios';
import Layout from '../components/Layout';
import Image from 'next/image';
import { AboutStats, TeamMember } from '../types';

const teamMembers: TeamMember[] = [
  { name: "Nirmal Patel", role: "Project Leader", imageSrc: "/images/Nirmal.png",commitKey: "nirmalCommits", issueKey: "nirmalIssues", description: "Nirmal Patel is a Physics senior pursuing Elements of Computing certificate. He took on the leadership role for CS331E project.",testKey: "3"},
  { name: "Alec Liu", role: "Full-stack Developer", imageSrc: "/images/Alec.png",commitKey: "alecCommits", issueKey: "alecIssues", description: "Alec is a Mechanical Engineering senior pursuing the Elements of Computing Certificate. He took on the full-stack role for the project.",testKey: "2"},
  { name: "Adrian Barajas", role: "Full-stack Developer", imageSrc: "/images/Adrian.png",commitKey: "adrianCommits", issueKey: "adrianIssues" ,description: "Adrian is a Mechanical Engineering senior pursuing Elements of Computing Certificate. He took on the full-stack role for CS331E project.",testKey: "2"},
  { name: "Junyu Yao", role: "Full-stack Developer", imageSrc: "/images/Junyu.png",commitKey: "junyuCommits", issueKey: "junyuIssues",description: "Junyu yao is a Physics senior pursuing Elements of Computing certificate. He took on the full-stack role for CS331E project. React developer. ",testKey: "2"},
];

const AboutPage: React.FC = () => {
  const [stats, setStats] = useState<AboutStats | null>(null);
  const [showStats, setShowStats] = useState(false);
  const [showData, setShowData] = useState(false);
  const [showTools, setShowTools] = useState(false);
  const [detailsVisibility, setDetailsVisibility] = useState<Record<string, boolean>>({});

  useEffect(() => {
    const fetchStats = async () => {
      try {
        const { data } = await axios.get<AboutStats>('http://localhost:8080/api/about');
        setStats(data);
      } catch (error) {
        console.error('Failed to fetch stats:', error);
      }
    };

     const visibilityState = teamMembers.reduce<Record<string, boolean>>((acc, member) => {
      acc[member.name] = false;
      return acc;
    }, {});

    setDetailsVisibility(visibilityState);
    fetchStats();
  }, []);

  
  const toggleDetails = (memberName: string) => {
    setDetailsVisibility(prev => ({ ...prev, [memberName]: !prev[memberName] }));
  };

  return (
    <>
      <Head>
        <title>About</title>
      </Head>
      <Layout>
        <main className="container py-4">
          <h1 className="text-center fw-light">YouTube statistics.</h1>
          <h1 className="text-center fw-light">Simplified.</h1>
          <br/>
          <p className="text-center lead text-body-secondary">Our website provides YouTube statistics based on
              channels, videos, and playlists.
          </p>
          <br />
          <br />
          <h1 className="text-center fw-light">Meet the team:</h1>
          
          <div className="row">
          {teamMembers.map((member, index) => (
            <div key={index} className="col-lg-3 col-md-6 col-sm-12">
              <div className="card mb-3 rounded-lg">
                <Image src={member.imageSrc} width={500} height={500} alt={member.name} layout='responsive' className='rounded-lg'/>
                <div className="card-body text-center">
                  <h5>{member.name}</h5>
                  <p>{member.role}</p>
                  <button onClick={() => toggleDetails(member.name)} className="btn btn-outline-secondary">
                    {detailsVisibility[member.name] ? 'Hide Details' : 'Show Details'}
                  </button>
                  {detailsVisibility[member.name] && (
                    <>
                      <br />
                      <p>{member.description}</p>
                      <p>#Commits: {stats ? stats[member.commitKey] : 'Loading...'}</p>
                      <p>#Issues: {stats ? stats[member.issueKey] : 'Loading...'}</p>
                      <p>#UnitTests: {member.testKey}</p>
                    </>
                  )}
                </div>
              </div>
            </div>
          ))}
        </div>
          {/* Stats Section */}
          <button className="btn btn-outline-secondary w-100 mb-3" type="button" onClick={() => setShowStats(!showStats)}>
            Stats
          </button>
          {showStats && stats && (
            <div className="card card-body mb-3">
              <ul>
                <li>Total #Commits: {stats.totalCommits}</li>
                <li>Total #Issues: {stats.totalIssues}</li>
                <li>Total #Unit Tests: 9</li>
                <li><Link href="https://gitlab.com/cs331e1/ytubedb/-/issues/?sort=created_date&state=all&first_page_size=20">Issue Tracker</Link></li>
                <li><Link href="https://gitlab.com/cs331e1/ytubedb">Gitlab Repo</Link></li>
                <li><Link href="https://gitlab.com/cs331e1/ytubedb/-/wikis/Technical-Report">Gitlab Wiki</Link></li>
                <li><Link href="https://red-trinity-290316.postman.co/workspace/f93ebbd0-e80d-43c9-8570-a6aaabda84bc">Postman API</Link></li>
              </ul>
            </div>
          )}

          {/* Data Section */}
          <button className="btn btn-outline-secondary w-100 mb-3" type="button" onClick={() => setShowData(!showData)}>
            Data
          </button>
          {showData && (
            <div className="card card-body mb-3">
              <ul>
                <li><Link href="https://documenter.getpostman.com/view/28025851/2sA35D74ac">Postman Collection</Link></li>
                <li><Link href="https://developers.google.com/youtube/v3/docs">Data Sources</Link></li>
              </ul>
            </div>
          )}

          {/* Tools Section */}
          <button className="btn btn-outline-secondary w-100 mb-3" type="button" onClick={() => setShowTools(!showTools)}>
            Tools
          </button>
          {showTools && (
            <div className="card card-body">
              <ul>
                <li>HTML</li>
                <li>CSS (Bootstrap)</li>
                <li>JavaScript</li>
                <li>React & Next.js</li>
                <li>Python (for any server-side logic)</li>
                <li>GitHub (version control)</li>
                {/* Add more tools as necessary */}
              </ul>
            </div>
          )}
        </main>
      </Layout>
    </>
  );
};

export default AboutPage;
