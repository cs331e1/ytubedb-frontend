import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import axios from 'axios';
import Layout from '../../components/Layout';
import { useRouter } from 'next/router';
import { Video } from '../../types'; // Ensure your type definition matches your data structure

const VideoPage = () => {
  const [video, setVideo] = useState<Video | null>(null);
  const [error, setError] = useState<string>('');
  const [playlists, setPlaylists] = useState<any[]>([]);
  const [showDescription, setShowDescription] = useState(false);
  const [showPlaylists, setShowPlaylists] = useState(false);
  const router = useRouter();
  const { videoId } = router.query;

  useEffect(() => {
    const fetchVideo = async () => {
      if (!videoId) return;
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/video/${videoId}`);
        if (!response.data || typeof response.data !== 'object') {
          setError('Video data is not in expected format or not found.');
          return;
        }
        setVideo(response.data.video);
        setPlaylists(response.data.playlists); // Assuming playlist data is included in your response
      } catch (error) {
        console.error('Error fetching video:', error);
        setError('Failed to fetch video.');
      }
    };

    fetchVideo();
  }, [videoId]);

  return (
    <>
      <Head>
        <title>{video ? video.title : 'Video Not Found'}</title>
      </Head>
      <Layout>
        <main className="py-5 mt-5">
          <div className="container">
            {error ? (
              <h1 className="fw-light text-center">Error: {error}</h1>
            ) : video ? (
              <>
                <h1 className="fw-light text-center">{video.title}</h1>
                <div className="card mb-3 d-flex justify-content-center align-items-center" style={{ overflow: 'hidden', borderRadius: '20px' }}>
                  <div className="" style={{ width: '80%', position: 'relative', aspectRatio: '950 / 534.38' }}>
                    <iframe
                      src={`https://www.youtube.com/embed/${video.videoID}`}
                      frameBorder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                      title={video.title}
                      style={{
                        position: 'absolute',
                        top: '0',
                        left: '0',
                        width: '100%',
                        height: '100%',
                        borderRadius: '20px'  // to ensure the iframe itself also has rounded corners if needed
                      }}
                    ></iframe>
                  </div>
                  
                  <div className="card-body">
                      <div className="mt-3">
                        <h5>Description</h5>
                        <div className="">{video.description}</div>
                      </div>
                    <div className="d-flex justify-content-between align-items-center mt-3">
                      <span><i className="far fa-eye"></i> {video.viewCount}</span>
                      <span><i className="far fa-thumbs-up"></i> {video.likeCount}</span>
                      <span><i className="far fa-comment"></i> {video.commentCount}</span>
                    </div>
                    <button className="btn btn-outline-secondary w-100 mt-3" type="button" onClick={() => setShowPlaylists(!showPlaylists)}>
                      View Playlists
                    </button>
                    {showPlaylists && (
                      <div className="mt-3">
                        {playlists.length > 0 ? (
                          <ul className="list-unstyled">
                            {playlists.map((playlist) => (
                              <li key={playlist.playlistID}>
                                <Link href={`/playlists/${playlist.playlistID}`}>
                                  <div className="ml-3 w-100 mt-3">{playlist.title}</div>
                                </Link>
                              </li>
                            ))}
                          </ul>
                        ) : (
                          <p>No playlists found for this video.</p>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </>
            ) : (
              <h1 className="fw-light text-center">Loading...</h1>
            )}
          </div>
        </main>
      </Layout>
    </>
  );
};

export default VideoPage;
