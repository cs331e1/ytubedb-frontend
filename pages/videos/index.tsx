import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Layout from '../../components/Layout';
import axios from 'axios';
import SearchFilterSort from '@/components/SearchFilterSort';
import Pagination from '@/components/Pagination';
import { Video } from '@/types';

const VideoPage: React.FC = () => {
  const [videos, setVideos] = useState<Video[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [searchTerm, setSearchTerm] = useState('');
  const [filter, setFilter] = useState('');
  const [minArg, setMinArg] = useState('');
  const [maxArg, setMaxArg] = useState('');
  const [sort, setSort] = useState('');
  const [sortOrder, setSortOrder] = useState('asc');

  useEffect(() => {
    const fetchVideos = async () => {
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/videos/${currentPage}`, {
          params: {
            search_arg: searchTerm,
            filter_arg: filter,
            filter_min_arg: minArg,
            filter_max_arg: maxArg,
            sort_arg: sort,
            sort_ord: sortOrder
          }
        });
        setVideos(response.data.videos);
        setTotalPages(response.data.total_pages);
      } catch (error) {
        console.error('Error fetching videos:', error);
        setVideos([]);
      }
    };

    fetchVideos();
  }, [currentPage, searchTerm, filter, minArg, maxArg, sort, sortOrder]);

  const handleSearchChange = (searchValue:string) => {
    setSearchTerm(searchValue);
  };

  const handleFilterChange = (filterValue:string, minValue:string='', maxValue:string='') => {
    setFilter(filterValue);
    setMinArg(minValue);
    setMaxArg(maxValue);
  };

  const handleSortChange = (sortValue:string) => {
    setSort(sortValue);
  };

  const handleSortOrderChange = (orderValue: 'asc' | 'desc') => {
    setSortOrder(orderValue);
  };

  return (
    <>
      <Head>
        <title>Videos List</title>
      </Head>
      <Layout>
        <main className="py-5 mt-5">
          <div className="container">
            <h1 className="fw-light text-center">YouTube Videos</h1>
            <SearchFilterSort
              onSearchChange={handleSearchChange}
              onFilterChange={handleFilterChange}
              onSortChange={handleSortChange}
              onSortOrderChange={handleSortOrderChange}
              filterOptions={[{value: 'views', label: 'Views'}, {value: 'likes', label: 'likes'}, {value: 'comments', label: 'comments'}]}
              sortOptions={[{value: 'title', label: 'title'}, {value: 'views', label: 'Views'}, {value: 'likes', label: 'likes'}, {value: 'comments', label: 'comments'}, {value: 'upload date', label: 'upload date'}]}
            />
            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              setCurrentPage={setCurrentPage}
            />
            <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-3 mt-3">
              {videos.map((video) => (
                <div className="col" key={video.videoID}>
                  <div className="card shadow-sm">
                    <Link href={`/videos/${video.videoID}`}>
                        <img src={video.thumbnail} alt="Video thumbnail" className="card-img-top" />
                    </Link>
                    <div className="card-body">
                    <h5 className="card-title" style={{
                        display: '-webkit-box',
                        WebkitLineClamp: 2,
                        WebkitBoxOrient: 'vertical',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        lineHeight: '1.5',
                        maxHeight: '3em', 
                        minHeight: '3em',
                      }}>{video.title}</h5>
                      <div className="d-flex justify-content-between align-items-center">
                      <Link href={`/channels/${video.channelID}`}>
                        <div className="d-flex align-items-center text-decoration-none text-muted">
                          <img src={video.channelThumbnail} alt={video.channelName} className="rounded-circle img-fluid" style={{ width: '40px', height: '40px' }}/>
                          <small className="ms-2">{video.channelName}</small>
                        </div>
                      </Link>
                      <small className="text-muted">
                        <i className="far fa-eye mr-2"></i>
                        {video.viewCount} views
                      </small>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className="d-flex justify-content-center mt-4">
            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              setCurrentPage={setCurrentPage}
            />
            </div>
          </div>
        </main>
      </Layout>
    </>
  );
};

export default VideoPage;
