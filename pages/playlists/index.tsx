import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import axios from 'axios';
import Layout from '../../components/Layout';
import SearchFilterSort from '../../components/SearchFilterSort';
import Pagination from '@/components/Pagination';
import { Playlist } from '../../types';


const PlaylistsPage: React.FC = () => {
  const [playlists, setPlaylists] = useState<Playlist[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [searchTerm, setSearchTerm] = useState('');
  const [filter, setFilter] = useState('');
  const [minArg, setMinArg] = useState('');
  const [maxArg, setMaxArg] = useState('');
  const [sort, setSort] = useState('');
  const [sortOrder, setSortOrder] = useState('asc');

  useEffect(() => {
    const fetchPlaylists = async () => {
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/playlists/${currentPage}`, {
          params: { search_arg: searchTerm, 
            filter_arg: filter, 
            filter_min_arg: minArg, 
            filter_max_arg: maxArg, 
            sort_arg: sort,
            sort_ord: sortOrder }
      });
        setPlaylists(response.data.playlists);
        setTotalPages(response.data.total_pages);
      } catch (error) {
        console.error('Error fetching playlists:', error);
        setPlaylists([]);
      }
    }
    fetchPlaylists();
  }, [currentPage, searchTerm, filter, minArg, maxArg, sort, sortOrder]);

  const handleSearchChange = (searchValue:string) => {
    setSearchTerm(searchValue);
  };

  const handleFilterChange = (filterValue:string, minValue:string='', maxValue:string='') => {
    setFilter(filterValue);
    setMinArg(minValue);
    setMaxArg(maxValue);
  };

  const handleSortChange = (sortValue:string) => {
    setSort(sortValue);
  };

  const handleSortOrderChange = (orderValue: 'asc' | 'desc') => {
    setSortOrder(orderValue);
  };

  const maxPageNumbersToShow = 3;
  let startPage = Math.max(currentPage - Math.floor(maxPageNumbersToShow / 2), 1);
  let endPage = startPage + maxPageNumbersToShow - 1;

  // Adjust if endPage exceeds total pages
  if (endPage > totalPages) {
    endPage = totalPages;
    // Ensure that we always display maxPageNumbersToShow (if possible)
    startPage = Math.max(1, endPage - maxPageNumbersToShow + 1);
  }

  const pageNumbers = [];
  for (let number = startPage; number <= endPage; number++) {
    pageNumbers.push(number);
  }

  return (
    <>
      <Head>
        <title>Playlist Page</title>
      </Head>
      <Layout>
        <main className="py-5 mt-5">
          <div className="container">
            <h1 className="fw-light text-center">Youtube Playlists</h1>
            <SearchFilterSort
              onSearchChange={handleSearchChange}
              onFilterChange={handleFilterChange}
              onSortChange={handleSortChange}
              onSortOrderChange={handleSortOrderChange}
              filterOptions={[{value: 'video count', label: 'video count'}, {value: 'total views', label: 'total views'}, {value: 'total likes', label: 'total likes'}, {value: 'total comments', label: 'total comments'}]}
              sortOptions={[{value: 'title', label: 'title'}, {value: 'creation date', label: 'creation date'}, {value: 'video count', label: 'video count'}, {value: 'total views', label: 'total views'}, {value: 'total likes', label: 'total likes'}, {value: 'total comments', label: 'total comments'}]}
            />
            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              setCurrentPage={setCurrentPage}
            />
            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 mt-3">
              {playlists.map((playlist) => (
                <div className="col" key={playlist.playlistID}>
                  <div className="card shadow-sm">
                    <Link href={`/playlists/${playlist.playlistID}`}>
                    <img src={playlist.thumbnail} alt={playlist.title} className="card-img-top" />
                    </Link>
                    <div className="card-body">
                    <h5 className="card-title" style={{
                        display: '-webkit-box',
                        WebkitLineClamp: 2,
                        WebkitBoxOrient: 'vertical',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        lineHeight: '1.5',
                        maxHeight: '3em', 
                        minHeight: '3em',
                      }}>{playlist.title}</h5>
                      <div className="d-flex justify-content-between align-items-center">
                      <Link href={`/channels/${playlist.channelID}`}>
                        <div className="d-flex align-items-center text-decoration-none text-muted">
                          <img src={playlist.channelThumbnail} alt={playlist.channelName} className="rounded-circle img-fluid" style={{ width: '40px', height: '40px' }}/>
                          <small className="ms-2">{playlist.channelName}</small>
                        </div>
                      </Link>
                      <small className="text-muted">
                        <i className="far fa-eye mr-2"></i>
                        {playlist.videoCount} views
                      </small>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className="d-flex justify-content-center mt-4">
            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              setCurrentPage={setCurrentPage}
            />
            </div>
          </div>
        </main>
      </Layout>
    </>
  );
};

export default PlaylistsPage;
