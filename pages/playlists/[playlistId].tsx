import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import axios from 'axios';
import Layout from '../../components/Layout';
import { useRouter } from 'next/router';
import { Playlist} from '../../types';

const PlaylistPage = () => {
  const [playlist, setPlaylist] = useState<Playlist | null>(null);
  const [error, setError] = useState('');
  const router = useRouter();
  const { playlistId } = router.query;
  const [videos, setVideos] = useState<any[]>([]);
  const [showVideos, setShowVideos] = useState(false);
  

  useEffect(() => {
    const fetchPlaylist = async () => {
      if (!playlistId) return;
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/playlist/${playlistId}`);
        if (!response.data || response.data.length === 0) {
          setError('Playlist not found.');
          return;
        }
        // Assuming your API directly returns the playlist object
        setPlaylist(response.data.playlist);
        setVideos(response.data.videos);
      } catch (error) {
        console.error('Error fetching playlist:', error);
        setError('Failed to fetch playlist.');
      }
    };

    fetchPlaylist();
  }, [playlistId]);

  return (  
    <>
      <Head>
        <title>{playlist ? playlist.title : 'Playlist Not Found'}</title>
      </Head>
      <Layout>
        <main className="py-5 mt-5">
          <div className="container">
            {error ? (
              <h1 className="fw-light text-center">{error}</h1>
            ) : playlist ? (
              <>
                <h1 className="fw-light text-center">{playlist.title}</h1>
                <div className="card mb-3">
                  <div className="d-flex justify-content-center mt-5">
                    <img src={playlist.thumbnail} alt="Playlist Thumbnail" className="img-fluid" style={{ maxWidth: '100%', height: 'auto', marginBottom: '20px', borderRadius: '8px', boxShadow: '0 4px 6px rgba(0,0,0,0.1)' }} />
                  </div>
                  <div className="card-body">
                    <div>
                      <h5>Description</h5>
                      <p>{playlist.description}</p>
                      <div className="d-flex justify-content-between align-items-center">
                          <span><i className="far fa-calendar-alt"></i> {playlist.publishedAt}</span>
                          <span><i className="far fa-eye"></i> {playlist.totalViews}</span>
                          <span><i className="far fa-thumbs-up"></i> {playlist.totalLikes}</span>
                          <span><i className="far fa-comment"></i> {playlist.totalComments}</span>
                      </div>
                    </div>
                    <button className="btn btn-outline-secondary w-100 mt-3" type="button" onClick={() => setShowVideos(!showVideos)}>
                    View Videos
                  </button>
                  {showVideos && (
                    <div className="accordion-body">
                      {videos.length > 0 ? (
                        <ul className="list-unstyled">
                          {videos.map((video) => (
                            <li key={video.videoID}>
                              <Link href={`/videos/${video.videoID}`}>
                                <div className="ml-3 w-100 mt-3">{video.title}</div>
                              </Link>
                            </li>
                          ))}
                        </ul>
                      ) : (
                        <p>No videos found for this channel.</p>
                      )}
                    </div>
                  )}
                    <div className="d-flex justify-content-around mt-3">
                      {playlist.channelID && (
                        <Link href={`/channels/${playlist.channelID}`}>
                          <button className="btn btn-outline-secondary">View Channel</button>
                        </Link>
                      )}
                      <a href={`https://www.youtube.com/playlist?list=${playlist.playlistID}`} target="_blank" rel="noopener noreferrer" className="btn btn-danger">Watch on YouTube</a>
                    </div>
                  </div>
                </div>
              </>
            ) : (
              <h1 className="fw-light text-center">Loading...</h1>
            )}
          </div>
        </main>
      </Layout>
    </>
  );
};


export default PlaylistPage;
