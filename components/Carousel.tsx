import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import Link from 'next/link';

type Thumbnail = {
  url: string;
};

type Snippet = {
  title: string;
  thumbnails: {
    high?: Thumbnail;
    medium?: Thumbnail;
  };
  itemType?: 'channel' | 'video' | 'playlist';
};

type Item = {
  id: string;
  snippet: Snippet;
};

type CarouselProps = {
  items: Item[];
  carouselId: string;
};

const MyCarousel: React.FC<CarouselProps> = ({ items, carouselId }) => {
  // Function to split items into chunks of 3
  const chunkItems = (items: Item[], size: number): Item[][] => {
    return items.reduce<Item[][]>((chunks, item, index) => {
      const chunkIndex = Math.floor(index / size);
      if (!chunks[chunkIndex]) {
        chunks[chunkIndex] = []; // Start a new chunk
      }
      chunks[chunkIndex].push(item);
      return chunks;
    }, []);
  };
  

  const getLinkUrl = (item: Item) => {
    switch (item.snippet.itemType) {
      case 'channel': return `/channels/${item.id}`;
      case 'video': return `/videos/${item.id}`;
      case 'playlist': return `/playlists/${item.id}`;
      default: return '#';
    }
  };

  const itemChunks = chunkItems(items, 3);

  return (
  <Carousel id={carouselId}>
    {itemChunks.map((chunk, idx) => (
      <Carousel.Item key={idx}>
        <div className="d-flex justify-content-center" style={{ margin: '0 auto' }}>
          {chunk.map((item) => (
            <Link key={item.id} href={getLinkUrl(item)} passHref legacyBehavior>
              <div className="d-block" style={{ width: '30%', margin: '0 10px' , display: 'flex', justifyContent: 'center', }}>
                <img
                  src={item.snippet.thumbnails.high?.url || item.snippet.thumbnails.medium?.url}
                  alt={item.snippet.title}
                  className="img-fluid"
                  style={{ objectFit: 'cover', height: '33vh' }}
                />
              </div>
            </Link>
          ))}
        </div>
      </Carousel.Item>
    ))}
  </Carousel>
);
} 

export default MyCarousel;
