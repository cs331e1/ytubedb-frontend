import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

type LayoutProps = {
  children: React.ReactNode;
};

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const router = useRouter();

  const isActive = (pathname: string) => router.pathname === pathname;

  return (
    <>
      <nav className="navbar navbar-expand sticky-top navbar-dark bg-dark" aria-label="Main navigation">
        <div className="container-fluid">
          <div className="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link href="/" className={`nav-link ${isActive('/') ? 'active' : ''}`} aria-current="page">YTubeDB</Link>
              </li>
              <li className="nav-item">
                <Link className={`nav-link ${isActive('/about') ? 'active' : ''}`} href="/about" >About</Link>
              </li>
              <li className="nav-item">
                <Link href="/channels" className={`nav-link ${isActive('/channels') ? 'active' : ''}`}>Channels</Link>
              </li>
              <li className="nav-item">
                <Link href="/videos" className={`nav-link ${isActive('/videos') ? 'active' : ''}`}>Videos</Link>
              </li>
              <li className="nav-item">
                <Link href="/playlists" className={`nav-link ${isActive('/playlists') ? 'active' : ''}`}>Playlists</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <main>{children}</main>
    </>
  );
};

export default Layout;
