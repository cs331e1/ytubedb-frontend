import React, { useState } from 'react';

interface Option {
  value: string;
  label: string;
}

interface SearchFilterSortProps {
  onSearchChange: (searchTerm: string) => void;
  onFilterChange: (filter: string, min?: string, max?: string) => void;
  onSortChange: (sortField: string) => void;
  onSortOrderChange: (sortOrder: 'asc' | 'desc') => void;
  filterOptions: Option[];
  sortOptions: Option[];
}

const SearchFilterSort: React.FC<SearchFilterSortProps> = ({
  onSearchChange,
  onFilterChange,
  onSortChange,
  onSortOrderChange,
  filterOptions,
  sortOptions
}) => {
    const [searchTerm, setSearchTerm] = useState('');
    const [filterArg, setFilterArg] = useState('');
    const [minArg, setMinArg] = useState('');
    const [maxArg, setMaxArg] = useState('');
    const [sort, setSort] = useState('');
    const [sortOrder, setSortOrder] = useState('asc');

    const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchTerm(e.target.value);
        onSearchChange(e.target.value);
    };

    const handleFilterChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setFilterArg(e.target.value);
        onFilterChange(e.target.value);
    };

    const applyFilter = () => {
        if (filterArg) {
            onFilterChange(filterArg, minArg, maxArg);
        }
    };

    const handleSortChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setSort(e.target.value);
        onSortChange(e.target.value);
    };

    const handleSortOrderChange = (order: 'asc' | 'desc') => {
        setSortOrder(order);
        onSortOrderChange(order);
    };

    return (
        <div className="d-flex justify-content-center align-items-center flex-column">
            {/* Search input and button */}
            <div className="input-group mb-3">
                <input
                    type="text"
                    className="form-control"
                    name="search"
                    placeholder="Search videos..."
                    value={searchTerm}
                    onChange={handleSearchChange}
                />
                <button type="button" className="btn btn-primary" onClick={() => onSearchChange(searchTerm)}>
                    <i className="fas fa-search"></i>
                </button>
            </div>

            {/* Filter options */}
            <div className="d-flex align-items-center mb-3">
                <label htmlFor="filterSelect" className="form-label me-2"><i className="fas fa-filter"></i></label>
                <select id="filterSelect" className="form-select me-2" onChange={handleFilterChange} value={filterArg}>
                    <option value="">Select filter</option>
                    {filterOptions.map((opt, index) => (
                        <option key={index} value={opt.value}>{opt.label}</option>
                    ))}
                </select>
                {filterArg && (
                    <>
                        <input
                            type="text"
                            className="form-control me-2"
                            placeholder="min"
                            value={minArg}
                            onChange={(e) => setMinArg(e.target.value)}
                        />
                        <input
                            type="text"
                            className="form-control me-2"
                            placeholder="max"
                            value={maxArg}
                            onChange={(e) => setMaxArg(e.target.value)}
                        />
                        <button onClick={applyFilter} className="btn btn-secondary">
                            <i className="fas fa-sliders-h"></i>
                        </button>
                    </>
                )}
            </div>

            {/* Sort options */}
            <div className="d-flex align-items-center mb-3">
                <label htmlFor="sortSelect" className="form-label me-2"><i className="fas fa-sort"></i></label>
                <select id="sortSelect" className="form-select me-2" onChange={handleSortChange} value={sort}>
                    <option value="">Select sort</option>
                    {sortOptions.map((opt, index) => (
                        <option key={index} value={opt.value}>{opt.label}</option>
                    ))}
                </select>
                <div className="btn-group" role="group" aria-label="Sort Order">
                    <button className={`btn btn-outline-secondary ${sortOrder === 'asc' ? 'active' : ''}`} onClick={() => handleSortOrderChange('asc')}>
                        <i className="fas fa-arrow-up"></i>
                    </button>
                    <button className={`btn btn-outline-secondary ${sortOrder === 'desc' ? 'active' : ''}`} onClick={() => handleSortOrderChange('desc')}>
                        <i className="fas fa-arrow-down"></i>
                    </button>
                </div>
            </div>
        </div>
    );
};

export default SearchFilterSort;
