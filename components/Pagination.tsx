import React from 'react';

interface PaginationProps {
  currentPage: number;
  totalPages: number;
  setCurrentPage: (pageNumber: number) => void;
}

const Pagination: React.FC<PaginationProps> = ({ currentPage, totalPages, setCurrentPage }) => {
  const maxPageNumbersToShow = 3;
  let startPage = Math.max(currentPage - Math.floor(maxPageNumbersToShow / 2), 1);
  let endPage = startPage + maxPageNumbersToShow - 1;

  if (endPage > totalPages) {
    endPage = totalPages;
    startPage = Math.max(1, endPage - maxPageNumbersToShow + 1);
  }

  const pageNumbers = [];
  for (let number = startPage; number <= endPage; number++) {
    pageNumbers.push(number);
  }

  return (
    <nav aria-label="Page navigation">
      <ul className="pagination justify-content-center">
        {/* Previous Page Button */}
        {currentPage > 1 && (
          <li className="page-item">
            <a onClick={() => setCurrentPage(currentPage - 1)} className="page-link" aria-label="Previous">
              <span aria-hidden="true">Previous</span>
            </a>
          </li>
        )}
        {/* First page number for quick access if not at the start */}
        {currentPage > 3 && (
            <li className="page-item">
            <a onClick={() => setCurrentPage(1)} className="page-link">1</a>
            </li>
        )}

        {/* Ellipsis if there are hidden pages on the left */}
        {currentPage > 3 && (
            <li className="page-item disabled">
            <span className="page-link">...</span>
            </li>
        )}

        {/* Page Numbers */}
        {Array.from({ length: Math.min(5, totalPages) }, (_, index) => {
            const pageNumber = Math.min(
            Math.max(currentPage - 2 + index, 1),
            totalPages - 4 + index
            );
            return (
            currentPage <= 3 ? index + 1 : // Handling for the first few pages
            totalPages - currentPage <= 2 ? totalPages - 4 + index : // Handling for the last few pages
            pageNumber // Normal case
            );
        }).map(number => (
            <li key={number} className={`page-item ${currentPage === number ? 'active' : ''}`}>
            <a onClick={() => setCurrentPage(number)} className="page-link">
                {number}
            </a>
            </li>
        ))}

        {/* Ellipsis if there are hidden pages on the right */}
        {currentPage < totalPages - 2 && (
            <li className="page-item disabled">
            <span className="page-link">...</span>
            </li>
        )}

        {/* Last page number for quick access if not at the end */}
        {currentPage < totalPages - 2 && (
            <li className="page-item">
            <a onClick={() => setCurrentPage(totalPages)} className="page-link">{totalPages}</a>
            </li>
        )}
        {/* Next Page Button */}
        {currentPage < totalPages && (
          <li className="page-item">
            <a onClick={() => setCurrentPage(currentPage + 1)} className="page-link" aria-label="Next">
              <span aria-hidden="true">Next</span>
            </a>
          </li>
        )}
      </ul>
    </nav>
  );
};

export default Pagination;
